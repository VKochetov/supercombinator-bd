val Http4sVersion = "0.21.14"
val LogbackVersion = "1.2.3"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

lazy val supcombstarter =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.psttf",
      // we target Scala 2.12 because of http4s: https://mvnrepository.com/artifact/org.http4s/http4s-blaze-client
      scalaVersion := "2.12.12",  // scala-steward:off
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "io.circe" %%% "circe-generic" % "0.13.0",
        "io.circe" %%% "circe-literal" % "0.13.0",
        "io.circe" %%% "circe-generic-extras" % "0.13.0",
        "io.circe" %%% "circe-parser" % "0.13.0",
        "com.outr" %%% "scribe" % "2.7.10",
      ),
      scalacOptions ++= Seq(
        "-Xlint",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-Ypartial-unification",
        "-language:higherKinds",
      ),
      scalafmtOnCompile := true,
    )
    .jvmSettings(Seq(
    ))

lazy val supcombstarterJS = supcombstarter.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      "jitpack" at "https://jitpack.io",
    ),
    libraryDependencies ++= Seq(
      "org.typelevel" %%% "mouse" % "0.24",
      "com.github.OutWatch.outwatch" %%% "outwatch" % "584f3f2c32",
    ),
    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("supcombstarter.js.SupCombStarterJS"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm
  )

lazy val supcombstarterJVM = supcombstarter.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(supcombstarterJS, Compile)).value,
    mappings.in(Universal) ++= webpack.in(Compile, fullOptJS).in(supcombstarterJS, Compile).value.map { f =>
      f.data -> s"assets/${f.data.getName}"
    },
    mappings.in(Universal) ++= Seq(
      (target in(supcombstarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "css" / "bootstrap.min.css" ->
        "assets/bootstrap/dist/css/bootstrap.min.css",
      (target in(supcombstarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "jquery" / "dist" / "jquery.slim.min.js" ->
        "assets/jquery/dist/jquery.slim.min.js",
      (target in(supcombstarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "popper.js" / "dist" / "umd/popper.min.js" ->
        "assets/popper.js/dist/umd/popper.min.js",
      (target in(supcombstarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "js/bootstrap.min.js" ->
        "assets/bootstrap/dist/js/bootstrap.min.js",
    ),
    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    mainClass in reStart := Some("supcombstarter.jvm.SupCombStarterJVM"),
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-core" % "2.1.1",
      "org.typelevel" %% "cats-effect" % "2.3.0",
      "org.typelevel" %% "mouse" % "0.24",
      "org.postgresql" % "postgresql" % "42.2.18",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "com.github.tminglei" %% "slick-pg" % "0.19.4",
      "com.github.tminglei" %% "slick-pg_circe-json" % "0.19.4",
      "org.flywaydb" % "flyway-core" % "7.3.2",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "com.github.pureconfig" %% "pureconfig" % "0.14.0",
      "org.slf4j" % "slf4j-nop" % "1.7.30",
    ),
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full),
  )

disablePlugins(RevolverPlugin)

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}

herokuAppName in Compile := "supercombinator-bd"

target in Compile := (target in(supcombstarterJVM, Compile)).value
