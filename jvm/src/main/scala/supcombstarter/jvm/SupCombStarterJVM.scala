package supcombstarter.jvm

import cats.effect._
import cats.implicits._
import supcombstarter.jvm.config.{AppConfig, SlickConfig, SyncConfig}
import supcombstarter.jvm.data.db.{SlickPgProfile, Transactor}
import supcombstarter.jvm.http._
import org.flywaydb.core.Flyway
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import pureconfig.ConfigSource
import scribe.{Level, Logger}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

object SupCombStarterJVM extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      appConfig <- SyncConfig.read[IO, AppConfig](ConfigSource.default)
      slickNativeConfig <- IO(
        DatabaseConfig.forConfig[SlickPgProfile]("slick.dbs.default"),
      )
      slickRawConfig = slickNativeConfig.config
      slickConfig <- SyncConfig.read[IO, SlickConfig](
        ConfigSource.fromConfig(slickRawConfig),
      )
      flyway =
        Flyway
          .configure()
          .dataSource(
            slickConfig.db.url,
            slickConfig.db.user,
            slickConfig.db.password,
          )
          .baselineOnMigrate(true)
          .outOfOrder(true)
          .ignoreMissingMigrations(true)
          .load()
      _ <- IO(flyway.migrate())
      exitCode <- app(appConfig, slickNativeConfig)
        .use(_ => IO.never)
        .as(ExitCode.Success)
    } yield exitCode

  private def app(
    appConfig: AppConfig,
    slickNativeConfig: DatabaseConfig[_ <: JdbcProfile],
  ): Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      transactor <- Transactor.fromDatabaseConfig[IO](slickNativeConfig)
      httpApp = (
        staticEndpoints.endpoints() <+> HelloEndpoints.endpoints() <+>
          new SCTermRoutes(transactor).routes
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS(httpApp))
        .resource
    } yield server

}
