package supcombstarter.jvm.core

import supcombstarter.jvm.core.inference.{Predef, TypeInferencer}
import supcombstarter.jvm.core.lambda.SCCore
import supcombstarter.jvm.models.lambda.{Abstraction, Application, Variable}

object Example extends App {

  val ex = SCCore.Lift(
    Abstraction(Variable("x"), Application(Variable("x"), Variable("y"))),
  )

  println(ex.toString)

  try println(
    TypeInferencer
      .typeOf(Predef.env, ex)
      .toString,
  )
  catch {
    case err: Exception => err.printStackTrace()
  }
}
