package supcombstarter.jvm.models.lambda

import cats.Order
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import org.http4s.EntityEncoder
import org.http4s.circe._

final case class Id[TRef, TId](value: TId) extends AnyVal {
  override def toString: String = value.toString
}

object Id {

  def of[TRef] = new IdConstructor[TRef]

  implicit def idOrder[TRef, TId: Order]: Order[Id[TRef, TId]] =
    Order.by(_.value)

  implicit def idEncoder[TRef, TId: Encoder]: Encoder[Id[TRef, TId]] =
    Encoder.instance[Id[TRef, TId]](_.value.asJson)

  implicit def idDecoder[TRef, TId: Decoder]: Decoder[Id[TRef, TId]] =
    Decoder[TId].map(Id.apply[TRef, TId])

  implicit def idEntityEncoder[F[_], TRef, TId: Encoder]
    : EntityEncoder[F, Id[TRef, TId]] =
    jsonEncoderOf[F, Id[TRef, TId]]

}

class IdConstructor[TRef] {
  def valued[TId](value: TId) = new Id[TRef, TId](value)
}
