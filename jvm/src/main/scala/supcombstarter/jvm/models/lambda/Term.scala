package supcombstarter.jvm.models.lambda

import cats.effect.Sync
import io.circe.generic.JsonCodec
import org.http4s.EntityDecoder
import org.http4s.circe._

@JsonCodec sealed trait Term

@JsonCodec final case class Variable(name: String) extends Term {
  override def toString(): String = name
}

@JsonCodec final case class Abstraction(variable: Variable, term: Term)
    extends Term {
  override def toString: String = s"($variable) -> $term"
}

@JsonCodec final case class Application(function: Term, argument: Term)
    extends Term {
  override def toString: String = s"$function $argument"
}

object Term {
  implicit def TermEntityDecoder[F[_]: Sync]: EntityDecoder[F, Term] =
    jsonOf[F, Term]
}
